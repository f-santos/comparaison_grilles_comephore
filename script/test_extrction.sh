####################################################################################################################################################################
#  
# DATA PREPARATION - SELECTION OF A SINGLE TIME STEP ON EACH GRID
# 
####################################################################################################################################################################


#############################AT METEO FRANCE#############################

cd data

#POUR LA GRILLE A 0.01 DEGRES
cdo seltimestep,1 /cnrm/mosca/USERS/poncetn/Documents/Data/Climate/COMEPHORE/NETCDF_0.01/pr_COMEPHORE0.01_reg_remapcon_comephore_1997-2019.nc pr_COMEPHORE0.01_reg_remapcon_comephore_1timestep.nc

#PUR LA GRILLE A 1KM
cdo seltimestep,1 /cnrm/mosca/USERS/poncetn/Documents/Data/Climate/COMEPHORE/NETCDF_1KM/pr_comephore_1km-1h_201912.nc pr_comephore_1km-1h_1timestep.nc


##TO RUN IT AT HOME, CHANGE WORKING DIRECTORY AND RUN IT FROM SXMOSCA